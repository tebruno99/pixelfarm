//
// GameLayer.m
// PixelFarm
//
// Created by Thomas Bruno on 7/28/12.
//
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "GameLayer.h"
#import "Constants.h"
#import "GameUtil.h"
#import "Plant.h"

@implementation GameLayer

@synthesize tileMap;


- (id)init
{
    self = [super init];
    if (self) {
		propertiesDict = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void)setGridVisible:(BOOL)visible {
	if(visible)
		[gridLayer setVisible:true];
	else
		[gridLayer setVisible:false];
}

-(BOOL)gridEnabled {
	if(gridLayer) {
		return TRUE;
	}
	
	return FALSE;
}

-(void)useTool:(ToolTypes)tool at:(CGPoint)position {
	// TODO: Audit
	CCTMXLayer *playLayer = [tileMap layerNamed:@"Play"];
	CGPoint tileLoc = [GameUtil getTMXPositionFromCCPosition:position withMap:tileMap];
	int GID = [playLayer tileGIDAt:tileLoc];
	CCLOG(@"Going To use Tool at: %f %f",position.x,position.y);
	
	if([GameUtil tileCheck:GID toolType:tool]) {
		CGPoint rTileLoc = [GameUtil getCCPositionFromTMXPosition:tileLoc withMap:tileMap];
		
		// TODO: Audit
		Plant *plant = nil;
		int centerModx = ((tileMap.tileSize.width/CC_CONTENT_SCALE_FACTOR()) * 0.5f);
		int centerMody = ((tileMap.tileSize.height/CC_CONTENT_SCALE_FACTOR()) * 0.5f);
		
		switch(tool) {
			case kToolPlantCorn:
				plant = [Plant plantWithPlantType:kPlantCorn at:ccp(rTileLoc.x + centerModx, rTileLoc.y + centerMody) withParent:self];
				break;
			case kToolPlantBroccoli:
				plant = [Plant plantWithPlantType:kPlantBroccoli at:ccp(rTileLoc.x + centerModx, rTileLoc.y + centerMody) withParent:self];
				break;
			case kToolPlantCucumber:
				plant = [Plant plantWithPlantType:kPlantCucumber at:ccp(rTileLoc.x + centerModx, rTileLoc.y + centerMody) withParent:self];
				break;
			case kToolPlantTomato:
				plant = [Plant plantWithPlantType:kPlantTomato at:ccp(rTileLoc.x + centerModx, rTileLoc.y + centerMody) withParent:self];
				break;
			case kToolPlantPotato:
				plant = [Plant plantWithPlantType:kPlantPotato at:ccp(rTileLoc.x + centerModx, rTileLoc.y + centerMody) withParent:self];
				break;
			case kToolPlantRedPepper:
				plant = [Plant plantWithPlantType:kPlantRedPepper at:ccp(rTileLoc.x + centerModx, rTileLoc.y + centerMody) withParent:self];
				break;
			case kToolPlantCarrot:
				plant = [Plant plantWithPlantType:kPlantCarrot at:ccp(rTileLoc.x + centerModx, rTileLoc.y + centerMody) withParent:self];
				break;
			case kToolHoe:
				[self plowTileAt:tileLoc withMap:tileMap];
				break;
		}
		
		
		
		// If a plant was successfully created set the tile properties to planted
		if(plant != nil) {
			
			// Get the properties for the location if properties don't exist create one.
			NSMutableDictionary *properties = [self getPropertiesForTileAt:tileLoc];
			if(!properties) {
				properties = [[NSMutableDictionary alloc] init];
				[self createProperties:properties forTileAt:tileLoc];
			}
			
			// Report what the property was previously if set.
			if([properties objectForKey:@"plant"]) {
				NSNumber *plantID = [properties objectForKey:@"plant"];
				CCLOG(@"Previous Plant Property was: %d",[plantID intValue]);
			} else {
				CCLOG(@"Previous Plant Property was: nil");
			}
			
			// Set the property "plant" to the id of the plant that was planted.
			[properties setObject:[NSNumber numberWithInt:plant.type] forKey:@"plant"];
		}
	}
}

-(BOOL)checkTool:(ToolTypes)tool at:(CGPoint)position {
	// TODO: Audit
	bool canUse = false;
	CGPoint tileP = [GameUtil getTMXPositionFromCCPosition:position withMap:tileMap];
	int GID = [[tileMap layerNamed:@"Play"] tileGIDAt:tileP];
	CCLOG(@"GameLayer::checkTool GID Was: %d", GID);
	// First check if the tool can even be used on this type of tile
	canUse = [GameUtil tileCheck:GID toolType:tool];
	
	if(canUse) {

		// Check the tile properties if a plant is already in the location.
		NSDictionary *tileProperty = [self getPropertiesForTileAt:tileP];
		if([tileProperty objectForKey:@"plant"]) {
			NSNumber *num = [tileProperty objectForKey:@"plant"];
			canUse = false;
			CCLOG(@"Plant Property Found! It was: %d",[num intValue]);
		} else {
			CCLOG(@"Plant Property was not Found!");
		}
	}
	
	return canUse;
}


#pragma mark - Tile Property Management

-(NSMutableDictionary*)getPropertiesForTileAt:(CGPoint)tileLoc {
	NSNumber *numX = [NSNumber numberWithInt:tileLoc.x];
	NSNumber *numY = [NSNumber numberWithInt:tileLoc.y];
	
	NSMutableDictionary *yDict = [propertiesDict objectForKey:numX];
	if(!yDict) {
		return nil;
	}
	
	NSMutableDictionary *property = [yDict objectForKey:numY];
	if(!property) {
		return nil;
	}
	
	return property;
}

-(void)createProperties:(NSMutableDictionary*)properties forTileAt:(CGPoint)tileLoc {
	NSNumber *numX = [NSNumber numberWithInt:tileLoc.x];
	NSNumber *numY = [NSNumber numberWithInt:tileLoc.y];
	
	NSMutableDictionary *yDict = [propertiesDict objectForKey:numX];
	if(!yDict) {
		yDict = [[NSMutableDictionary alloc] init];
		[propertiesDict setObject:yDict forKey:numX];
	}
	
	[yDict setObject:properties forKey:numY];
}

#pragma mark - Plowing Controls

// Used to plow a tile.  Makes no safety checks! so be sure the tile can be plowed.
-(void)plowTileAt:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map {
	CCTMXLayer *playLayer = [map layerNamed:@"Play"];
	[playLayer setTileGID:kTileCenter at:TMXPosition];
	[self resolveTileEdges:TMXPosition withMap:map];
}

// Used to Unplow a tile. This makes sure that that a tile can actually be unplowed
-(void)unPlowTileAt:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map {
	CCTMXLayer *playLayer = [map layerNamed:@"Play"];
	
	if([GameUtil isTilePlowed:TMXPosition withMap:map]) {
		[playLayer setTileGID:kTileHole at:TMXPosition];
		[self unResolveTileEdges:TMXPosition withMap:map];
	} else if ([playLayer tileGIDAt:TMXPosition] == kTileHole) {
		[playLayer setTileGID:kTileGrass1 at:TMXPosition];
	}
}


// Used rebuild the edges around a tile that has been plowed
-(void)resolveTileEdges:(CGPoint)TMXPosition withMap:map {
	CCTMXLayer *layer = [map layerNamed:@"Play"];
	
	// Get N, S, W, E tiles and the tile next to them.
	CGPoint northTilePos = [GameUtil getTMXPositionFor:kDirectionNorth fromTileAt:TMXPosition withMap:map];
	CGPoint northEastTilePos = [GameUtil getTMXPositionFor:kDirectionNorthEast fromTileAt:TMXPosition withMap:map];
	CGPoint northWestTilePos = [GameUtil getTMXPositionFor:kDirectionNorthWest fromTileAt:TMXPosition withMap:map];
	CGPoint southTilePos = [GameUtil getTMXPositionFor:kDirectionSouth fromTileAt:TMXPosition withMap:map];
	CGPoint southEastTilePos = [GameUtil getTMXPositionFor:kDirectionSouthEast fromTileAt:TMXPosition withMap:map];
	CGPoint southWestTilePos = [GameUtil getTMXPositionFor:kDirectionSouthWest fromTileAt:TMXPosition withMap:map];
	CGPoint eastTilePos = [GameUtil getTMXPositionFor:kDirectionEast fromTileAt:TMXPosition withMap:map];
	CGPoint westTilePos = [GameUtil getTMXPositionFor:kDirectionWest fromTileAt:TMXPosition withMap:map];
	
	int northTile = [layer tileGIDAt:northTilePos];
	int northEastTile = [layer tileGIDAt:northEastTilePos];
	int northWestTile = [layer tileGIDAt:northWestTilePos];
	int southTile = [layer tileGIDAt:southTilePos];
	int southEastTile = [layer tileGIDAt:southEastTilePos];
	int southWestTile = [layer tileGIDAt:southWestTilePos];
	int eastTile = [layer tileGIDAt:eastTilePos];
	int westTile = [layer tileGIDAt:westTilePos];
	
	[layer setTileGID:[GameUtil resolveTile:kDirectionNorthWest currentGID:northWestTile] at:northWestTilePos];
	[layer setTileGID:[GameUtil resolveTile:kDirectionNorth currentGID:northTile] at:northTilePos];
	[layer setTileGID:[GameUtil resolveTile:kDirectionNorthEast currentGID:northEastTile] at:northEastTilePos];
	[layer setTileGID:[GameUtil resolveTile:kDirectionWest currentGID:westTile] at:westTilePos];
	[layer setTileGID:[GameUtil resolveTile:kDirectionEast currentGID:eastTile] at:eastTilePos];
	[layer setTileGID:[GameUtil resolveTile:kDirectionSouthWest currentGID:southWestTile] at:southWestTilePos];
	[layer setTileGID:[GameUtil resolveTile:kDirectionSouth currentGID:southTile] at:southTilePos];
	[layer setTileGID:[GameUtil resolveTile:kDirectionSouthEast currentGID:southEastTile] at:southEastTilePos];
}


// Used to restore the edges of a tile that has been unplowed.
-(void)unResolveTileEdges:(CGPoint)TMXPosition withMap:map {
	CCTMXLayer *layer = [map layerNamed:@"Play"];
	
	// Get N, S, W, E tiles and the tile next to them.
	CGPoint northTilePos = [GameUtil getTMXPositionFor:kDirectionNorth fromTileAt:TMXPosition withMap:map];
	CGPoint northEastTilePos = [GameUtil getTMXPositionFor:kDirectionNorthEast fromTileAt:TMXPosition withMap:map];
	CGPoint northWestTilePos = [GameUtil getTMXPositionFor:kDirectionNorthWest fromTileAt:TMXPosition withMap:map];
	CGPoint southTilePos = [GameUtil getTMXPositionFor:kDirectionSouth fromTileAt:TMXPosition withMap:map];
	CGPoint southEastTilePos = [GameUtil getTMXPositionFor:kDirectionSouthEast fromTileAt:TMXPosition withMap:map];
	CGPoint southWestTilePos = [GameUtil getTMXPositionFor:kDirectionSouthWest fromTileAt:TMXPosition withMap:map];
	CGPoint eastTilePos = [GameUtil getTMXPositionFor:kDirectionEast fromTileAt:TMXPosition withMap:map];
	CGPoint westTilePos = [GameUtil getTMXPositionFor:kDirectionWest fromTileAt:TMXPosition withMap:map];
	
	int northTile = [layer tileGIDAt:northTilePos];
	int northEastTile = [layer tileGIDAt:northEastTilePos];
	int northWestTile = [layer tileGIDAt:northWestTilePos];
	int southTile = [layer tileGIDAt:southTilePos];
	int southEastTile = [layer tileGIDAt:southEastTilePos];
	int southWestTile = [layer tileGIDAt:southWestTilePos];
	int eastTile = [layer tileGIDAt:eastTilePos];
	int westTile = [layer tileGIDAt:westTilePos];
	
	[layer setTileGID:[GameUtil unResolveTile:kDirectionNorthWest currentGID:northWestTile] at:northWestTilePos];
	[layer setTileGID:[GameUtil unResolveTile:kDirectionNorth currentGID:northTile] at:northTilePos];
	[layer setTileGID:[GameUtil unResolveTile:kDirectionNorthEast currentGID:northEastTile] at:northEastTilePos];
	[layer setTileGID:[GameUtil unResolveTile:kDirectionWest currentGID:westTile] at:westTilePos];
	[layer setTileGID:[GameUtil unResolveTile:kDirectionEast currentGID:eastTile] at:eastTilePos];
	[layer setTileGID:[GameUtil unResolveTile:kDirectionSouthWest currentGID:southWestTile] at:southWestTilePos];
	[layer setTileGID:[GameUtil unResolveTile:kDirectionSouth currentGID:southTile] at:southTilePos];
	[layer setTileGID:[GameUtil unResolveTile:kDirectionSouthEast currentGID:southEastTile] at:southEastTilePos];
}

@end

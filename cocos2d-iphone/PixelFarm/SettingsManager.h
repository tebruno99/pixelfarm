//
//  SettingsManager.h
//
//  http://getsetgames.com/2009/10/07/saving-and-loading-user-data-and-preferences/
//

#import <Foundation/Foundation.h>

@interface SettingsManager : NSObject {
	NSMutableDictionary* settings;
}





-(NSString *)getString:(NSString*)value;
-(NSString *)getString:(NSString*)value defaultString:(NSString*)defaultValue;

-(int)getInt:(NSString*)value;
-(int)getInt:(NSString*)value defaultInt:(int)defaultValue;

-(void)setValue:(NSString*)value newString:(NSString *)aValue;
-(void)setValue:(NSString*)value newInt:(int)aValue;

-(void)save;
-(void)load;

-(void)logSettings;

+(id)sharedSettingsManager;
@end
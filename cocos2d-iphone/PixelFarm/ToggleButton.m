//
//  ToggleButton.m
//  PixelFarm
//
// Copyright (c) 2012 Thomas Bruno
// Copyright (c) 2012 NaveOSS.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "ToggleButton.h"

@implementation ToggleButton

+(ToggleButton*)toggleButtonWithUp:(NSString*)upFrameName andDown:(NSString*)downFrameName {
	return [[ToggleButton alloc] initWithUp:upFrameName andDown:downFrameName];
	
}

-(id)initWithUp:(NSString*)upFrameName andDown:(NSString*)downFrameName {
	self = [super init];
	if(self != nil) {
	
		upFrame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:upFrameName];
		downFrame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:downFrameName];
		statusUp = true;
		
		[self setDisplayFrame:upFrame];
		
	}
	
	return self;
}

-(void)setUpFrame:(NSString*)name {
	upFrame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:name];
}

-(void)setDownFrame:(NSString*)name {
	downFrame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:name];
}

-(void)setButtonState:(BOOL)up withSound:(BOOL)on {
	if(up) {
		statusUp = true;
		[self setDisplayFrame:upFrame];
	} else {
		statusUp = false;
		[self setDisplayFrame:downFrame];
	}
}

-(void)toggle {
	if(statusUp) {
		[self setButtonState:false withSound:true];
	} else {
		[self setButtonState:true withSound:YES];
	}
}

-(bool)isUp {
	return statusUp;
}

@end

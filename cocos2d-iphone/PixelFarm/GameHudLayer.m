//
//  GameHudLayer.m
//  PixelFarm
//
//  Created by Thomas Bruno on 8/8/12.
//  Copyright (c) 2012 NaveOSS. All rights reserved.
//

#import "GameHudLayer.h"
#import "GameUtil.h"

#import "GameLayer.h"
#import "ToolCursor.h"

#define CircleMenuTag 1001
#define ToggleButtonIcon 1002

@implementation GameHudLayer

@synthesize gameLayer;



-(id)initGameHudWithGameLayer:(GameLayer *)gl {
	self = [super init];
	if(self != nil) {
		self.isTouchEnabled = YES;
		closeNextTouch = NO;
		gridActive = false;
		gameLayer = gl;
		selectedPos = CGPointZero;
		
		CircleMenu *menu = [[CircleMenu alloc] init];
		menu.delegate = self;
		[self addChild:menu z:1 tag:CircleMenuTag];

		[self setUpUIButtons];
	}
	return self;
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	for(UITouch *touch in touches) {
		CGPoint loc = [touch locationInView:[touch view]];
		loc = [[CCDirector sharedDirector] convertToGL:loc];

	}
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	CCLOG(@"Got Touch in GameHudLayer");
	for(UITouch *touch in touches) {
		CGPoint loc = [touch locationInView:[touch view]];
		loc = [[CCDirector sharedDirector] convertToGL:loc];
		CGPoint tmxPos = [GameUtil getTMXPositionFromCCPosition:loc withMap:[gameLayer tileMap]];
		CGPoint centerCCPos = [GameUtil getCenterOfTileCCPositionFromTMXPosition:tmxPos withMap:[gameLayer tileMap]];


		CircleMenu *circleMenu = (CircleMenu*)[self getChildByTag:CircleMenuTag];
		if(![circleMenu isOpen]) {
			self.isTouchEnabled = false;
			selectedPos = loc;
			[circleMenu setPosition:centerCCPos];
			[circleMenu showMenuWithAnimation:true duration:0.25f];
		}
	}
}

-(void)setUpUIButtons {
	ToggleButton *toggle = [ToggleButton toggleButtonWithUp:@"GridMenuButtonUp.png" andDown:@"GridMenuButtonDown.png"];
	[toggle setButtonState:true withSound:false];
	
	[self addChild:toggle z:0 tag:ToggleButtonIcon];
	[toggle setPosition:ccp(960,720)];
	[toggle setScale:1.25f];
}

#pragma mark - CircleMenu Delegate

-(void)circleMenuToolWasSelected:(ToolTypes)tool sender:(CircleMenu *)circleMenu {
	CCLOG(@"Tool Was Selected: %d",tool);

	[circleMenu hideMenuWithAnimation:true duration:0.25f];
	if([gameLayer checkTool:tool at:selectedPos]) {
		[gameLayer useTool:tool at:selectedPos];
	}
}

-(void)circleMenuWasOpened {

}

-(void)circleMenuWasClosed {
	CircleMenu *circleMenu = (CircleMenu*)[self getChildByTag:CircleMenuTag];
	self.isTouchEnabled = true;
	selectedPos = CGPointZero;
	[circleMenu setPosition:selectedPos];
}

-(void)circleMenuShouldCloseWithSender:(CircleMenu *)circleMenu {
	CCLOG(@"Circle menu Should Close With Sender Delegate Called!");
	[circleMenu hideMenuWithAnimation:true duration:0.25f];
}

@end

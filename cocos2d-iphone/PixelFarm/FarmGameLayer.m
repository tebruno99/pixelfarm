//
// FarmGameLayer.m
// PixelFarm
//
// Created by Thomas Bruno on 7/28/12.
//
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//


#import "cocos2d.h"
#import "GameUtil.h"
#import "FarmGameLayer.h"
#import "Plant.h"
#import "GameUtil.h"

@implementation FarmGameLayer

- (id)init
{
    self = [super init];
    if (self) {
		tileMap = [CCTMXTiledMap tiledMapWithTMXFile:@"GamePlayMap.tmx"];
		gridLayer = [tileMap layerNamed:@"Grid"];

		plants = [[NSMutableArray alloc] init];
		
		[self addChild:tileMap z:-1];
		
    }
    return self;
}

-(Plant*)plantInTile:(CGPoint)tilePoint {
	// TODO: Audit
	Plant *plant = nil;
	
	CGPoint realPoint = [GameUtil getCCPositionFromTMXPosition:tilePoint withMap:tileMap];
	CGPoint positionPoint = ccp(realPoint.x + ((tileMap.tileSize.width/CC_CONTENT_SCALE_FACTOR()) * 0.5f),realPoint.y + ((tileMap.tileSize.width/CC_CONTENT_SCALE_FACTOR()) * 0.5f));
	
	for (Plant* p in plants) {
		CGRect box = [p boundingBox];
		
		if(CGRectContainsPoint([p boundingBox], positionPoint)) {
			plant = p;
		}
	}
	
   return plant;
}

-(void) drawRect:(CGRect)rect
{
    // The rect is drawn using four lines
    CGPoint pos1, pos2, pos3, pos4;
    pos1 = CGPointMake(rect.origin.x, rect.origin.y);
    pos2 = CGPointMake(rect.origin.x, rect.origin.y + rect.size.height);
    pos3 = CGPointMake(rect.origin.x + rect.size.width,
					   rect.origin.y + rect.size.height);
    pos4 = CGPointMake(rect.origin.x + rect.size.width, rect.origin.y);
	
    ccDrawLine(pos1, pos2);
    ccDrawLine(pos2, pos3);
    ccDrawLine(pos3, pos4);
    ccDrawLine(pos4, pos1);
}



-(void) drawLine:(CGPoint)source to:(CGPoint)dest
{
    // The rect is drawn using four lines
    ccDrawLine(source, dest);
}


//-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//	for(UITouch *touch in touches) {
//		CGPoint touchLoc = [touch locationInView:[touch view]];
//		touchLoc = [[CCDirector sharedDirector] convertToGL:touchLoc];
//		CCLOG(@"Got Touch");
//
//		CGPoint touchedTileLoc = [GameUtil getTMXPositionFromCCPosition:touchLoc withMap:tileMap];
//		[GameUtil reportTileID:touchedTileLoc withMap:tileMap];
//
//		if([GameUtil canTileBePlowed:touchedTileLoc withMap:tileMap]) {
//			[self plowTileAt:touchedTileLoc withMap:tileMap];
//		} else if ([GameUtil isTilePlowed:touchedTileLoc withMap:tileMap] || [[tileMap layerNamed:@"Play"] tileGIDAt:touchedTileLoc] == kTileHole) {
//			[self unPlowTileAt:touchedTileLoc withMap:tileMap];
//		}
//	}
//}


@end
